﻿using Management.DataAccess;
using Management.Entities;
using Newtonsoft.Json;
using System;
using System.Net.NetworkInformation;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;

namespace Management.BusinessLogic
{
    public class BUS_Storage
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint UserMaxLength = 30;

        public static Storage[] ReadList(string sKeyword, uint Filter)
        {
            /* Read all storage */
            Storage[] storageList = DAL_Storage.ReadList(sKeyword);

            /* In case need filter unexpired product */
            if (1 == Filter)
            {
                /* Count the number of unexpired products  */
                DateOnly dateNow = DateOnly.FromDateTime(DateTime.Now);
                uint count = 0;
                for (uint i = 0; i < storageList.Length; i++)
                {
                    if (0 > dateNow.CompareTo(storageList[i].ExpiryDate))
                    {
                        count++;
                    }
                }

                /* Get unexpiry list */
                Storage[] unexpiryList = new Storage[count];
                count = 0;
                for (uint i = 0; i < storageList.Length; i++)
                {
                    if (0 > dateNow.CompareTo(storageList[i].ExpiryDate))
                    {
                        unexpiryList[count] = storageList[i];
                        count++;
                    }
                }

                return unexpiryList;
            }

            /* In case need filter expired product */
            if (2 == Filter)
            {
                /* Count the number of expired products  */
                DateOnly dateNow = DateOnly.FromDateTime(DateTime.Now);
                uint count = 0;
                for (uint i = 0; i < storageList.Length; i++)
                {
                    if (0 < dateNow.CompareTo(storageList[i].ExpiryDate))
                    {
                        count++;
                    }
                }

                /* Get expiry list */
                Storage[] expiryList = new Storage[count];
                count = 0;
                for (uint i = 0; i < storageList.Length; i++)
                {
                    if (0 < dateNow.CompareTo(storageList[i].ExpiryDate))
                    {
                        expiryList[count] = storageList[i];
                        count++;
                    }
                }

                return expiryList;
            }

            /* Return list */
            return storageList;
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            DAL_Storage.UpdateList(productOld, productNew);
        }

        public static string StringToImportStorage(string sProductName, string sQuantity, string sManufactureDate, string sExpiryDate, ref Storage storage)
        {
            /* Check for valid Product name */
            storage.ProductName = sProductName;
            Product[] productList = BUS_Product.ReadList("");
            bool isValid = false;
            for (int i = 0; i < productList.Length; i++)
            {
                if (storage.ProductName == productList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Product does not exist";
            }

            /* Check for valid Quantity */
            if (false == uint.TryParse(sQuantity, out storage.Quantity))
            {
                return "Quantity is invalid";
            }
            if (storage.Quantity == 0)
            {
                return "Quantity is invalid";
            }

            /* Check for valid Manufacture Date */
            if (false == DateOnly.TryParse(sManufactureDate, out storage.ManufactureDate))
            {
                return "Manufacture Date is invalid";
            }

            /* Check for valid Expiry Date */
            if (false == DateOnly.TryParse(sExpiryDate, out storage.ExpiryDate))
            {
                return "Expiry Date is invalid";
            }

            /* Manufacture date must be earlier than Expiry date */
            if (storage.ManufactureDate.CompareTo(storage.ExpiryDate) >= 0)
            {
                return "Manufacture date must be earlier than Expiry date";
            }

            /* Returns an empty string in case of a valid Storage */
            return string.Empty;
        }

        public static string StringToExportStorage(string sProductName, string sQuantity, string sManufactureDate, string sExpiryDate, ref Storage storage)
        {         
            /* Check for valid Product name */
            storage.ProductName = sProductName;
            Product[] productList = BUS_Product.ReadList("");
            bool isValid = false;
            for (int i = 0; i < productList.Length; i++)
            {
                if (storage.ProductName == productList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Product does not exist";
            }

            /* Check for valid Quantity */
            if (false == uint.TryParse(sQuantity, out storage.Quantity))
            {
                return "Quantity is invalid";
            }
            if (storage.Quantity == 0)
            {
                return "Quantity is invalid";
            }

            /* Check for valid Manufacture Date */
            if (false == DateOnly.TryParse(sManufactureDate, out storage.ManufactureDate))
            {
                return "Manufacture Date is invalid";
            }

            /* Check for valid Expiry Date */
            if (false == DateOnly.TryParse(sExpiryDate, out storage.ExpiryDate))
            {
                return "Expiry Date is invalid";
            }

            /* Manufacture date must be earlier than Expiry date */
            if (storage.ManufactureDate.CompareTo(storage.ExpiryDate) >= 0)
            {
                return "Manufacture date must be earlier than Expiry date";
            }

            /* Read all storage list */
            Storage[] storageList = ReadList("", 0);

            /* Check the product is in stock */
            isValid = false;
            for (uint i = 0; i < storageList.Length; i ++)
            {
                if (storage.ProductName == storageList[i].ProductName &&
                    storage.ManufactureDate == storageList[i].ManufactureDate &&
                    storage.ExpiryDate == storageList[i].ExpiryDate)
                {
                    /* Set flag to confirm the product is in stock */
                    isValid = true;

                    /* Check quantity*/
                    if (storage.Quantity > storageList[i].Quantity)
                    {
                        return "Quantity of product is not enough";
                    }

                    break;
                }
            }

            if (false == isValid)
            {
                return "Product is out of stock";
            }

            /* Returns an empty string in case of a valid Storage */
            return string.Empty;
        }

        public static string AddToList(Storage storage, ref Storage[] storageList)
        {
            bool addFlag = false;
            /* Add a storage with a product that already exists in the list */
            for (uint i = 0; i < storageList.Length; i++)
            {
                if (storage.ProductName == storageList[i].ProductName &&
                    storage.ManufactureDate == storageList[i].ManufactureDate &&
                    storage.ExpiryDate == storageList[i].ExpiryDate)
                {
                    storageList[i].Quantity += storage.Quantity;
                    addFlag = true;
                }
            }

            /* Add a storage with a new product */
            if (false == addFlag)
            {
                Storage[] storageListNew = new Storage[storageList.Length + 1];
                for (uint i = 0; i < storageList.Length; i++)
                {
                    storageListNew[i] = storageList[i];
                }
                storageListNew[storageList.Length] = storage;
                storageList = storageListNew;
            }

            /* Returns an empty string in case of successful addition */
            return string.Empty;
        }

        public static void StringToList(string sStorageList, ref Storage[] storageList)
        {
            /* Count the number of storage in the Storage List string */
            StringReader reader = new StringReader(sStorageList);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read storage list from the string */
            storageList = new Storage[count];
            reader = new StringReader(sStorageList);
            for (int i = 0; i < storageList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    storageList[i] = JsonConvert.DeserializeObject<Storage>(sData);
                }
            }
            reader.Close();
        }

        public static string ToStringList(Storage[] storageList)
        {
            /* Write storage to a string */
            StringWriter writer = new StringWriter();
            foreach (Storage r in storageList)
            {
                string sData = JsonConvert.SerializeObject(r);
                writer.WriteLine(sData);
            }
            string sResult = writer.ToString();
            writer.Close();

            /* Returns storage list as a string */
            return sResult;
        }

        public static void Import(Import import)
        {
            DAL_Storage.Import(import);
        }

        public static void Export(Export export)
        {
            DAL_Storage.Export(export);
        }
    }
}
