﻿using Management.BusinessLogic;
using Management.DataAccess;
using Management.Entities;

namespace Management.BusinessLogic
{
    public class BUS_Category
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint NameMaxLength = 30;

        public static Category Empty()
        {
            return new Category() { ID = "", Name = "" };
        }

        public static Category[] ReadList(string sKeyword)
        {
            /* Return category list */
            return DAL_Category.ReadList(sKeyword);
        }

        public static string Add(Category category)
        {
            /* Check the validity of the category */
            if (category.ID.Length > IDMaxLength)
            {
                return "Invalid category ID";
            }
            if (category.Name.Length > NameMaxLength)
            {
                return "Invalid category name";
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Category.Add(category);
        }
        public static string Edit(string sID, string sName, ref Category categoryOld)
        {
            Category category;
            category.ID = sID;
            category.Name = sName;

            /* Check for valid Category ID length */
            if (category.ID.Length == 0 || category.ID.Length > IDMaxLength)
            {
                return "Category ID is invalid";
            }

            /* Check for valid Category name length */
            if (category.Name.Length == 0 || category.Name.Length > NameMaxLength)
            {
                return "Category name is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Category.Edit(categoryOld, category);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Update product list */
                BUS_Product.UpdateCategory(categoryOld, category);

                /* Update category old */
                categoryOld = category;
            }

            return sInfo;
        }

        public static string Delete(string sID, string sName)
        {
            Category category;
            category.ID = sID;
            category.Name = sName;

            /* Check for valid category ID length */
            if (category.ID.Length == 0 || category.ID.Length > IDMaxLength)
            {
                return "Category ID is invalid";
            }

            /* Check for valid category name length */
            if (category.ID.Length == 0 || category.ID.Length > NameMaxLength)
            {
                return "Category name is invalid";
            }

            /* Check all products under the category are used in orders */
            Product[] productList = BUS_Product.ReadList("");
            for (uint i = 0; i < productList.Length; i++)
            {
                if (category.Name == productList[i].Category)
                {
                    /* Check products used in import orders */
                    if (string.IsNullOrEmpty(BUS_Import.ProductIsExist(productList[i])))
                    {
                        return "Unable to delete category has products used in import orders";
                    }

                    /* Check products used in export orders */
                    if (string.IsNullOrEmpty(BUS_Export.ProductIsExist(productList[i])))
                    {
                        return "Unable to delete category has products used in export orders";
                    }
                }
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Category.Delete(category);
        }

        public static Category? ReadInfo(string categoryCode)
        {
            return DAL_Category.ReadInfo(categoryCode);
        }
    }
}
