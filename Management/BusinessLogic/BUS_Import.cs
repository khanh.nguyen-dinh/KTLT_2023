﻿using Management.DataAccess;
using Management.Entities;
using Newtonsoft.Json;
using System;
using System.Net.NetworkInformation;

namespace Management.BusinessLogic
{
    public class BUS_Import
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint UserMaxLength = 30;

        public static Import[] ReadList(string sKeyword)
        {
            /* Return import list */
            return DAL_Import.ReadList(sKeyword);
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            DAL_Import.UpdateList(productOld, productNew);
        }

        public static string ProductIsExist(Product product)
        {
            /* Read import list */
            Import[] importList = ReadList("");

            /* Check product name in list */
            for (uint i = 0; i < importList.Length; i ++)
            {
                for (uint k = 0; k < importList[i].Storage.Length; k++)
                {
                    if (product.Name == importList[i].Storage[k].ProductName)
                    {
                        return string.Empty;
                    }
                }
            }

            /* product not exist */
            return "The Product is not exists";
        }

        public static string Add(string sID, string sUser, string sDate, Storage[] storageList, ref Import import)
        {
            import.ID = sID;
            import.User = sUser;
            import.Storage = storageList;

            /* Check for valid Import ID length */
            if (import.ID.Length == 0 || import.ID.Length > IDMaxLength)
            {
                return "Import ID is invalid";
            }

            /* Check for valid Import User length */
            if (import.User.Length == 0 || import.User.Length > IDMaxLength)
            {
                return "Import ID is invalid";
            }

            /* Check for valid Import Date */
            if (false == DateOnly.TryParse(sDate, out import.Date))
            {
                return "Import Date is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Import.Add(import);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Add import order to storage list */
                BUS_Storage.Import(import);
            }

            return string.Empty;
        }

        public static string Edit(string sID, string sUser, String sDate, ref Import importOld)
        {
            Import import = new Import();
            import.ID = sID;
            import.User = sUser;

            /* Check for valid Import ID length */
            if (import.ID.Length == 0 || import.ID.Length > IDMaxLength)
            {
                return "Import ID is invalid";
            }

            /* Check for valid Import User length */
            if (import.User.Length == 0 || import.User.Length > IDMaxLength)
            {
                return "Import ID is invalid";
            }

            /* Check for valid Import Date */
            if (false == DateOnly.TryParse(sDate, out import.Date))
            {
                return "Import Date is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Import.Edit(importOld, import);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Update Import old */
                importOld = import;
            }

            return sInfo;
        }

        public static string Delete(Import import)
        {
            /* Returns an empty string in case of successful addition */
            return DAL_Import.Delete(import);
        }

        public static Import? ReadInfo(string importID)
        {
            return DAL_Import.ReadInfo(importID);
        }
    }
}
