﻿using Management.BusinessLogic;
using Management.DataAccess;
using Management.Entities;

namespace Management.BusinessLogic
{
    public class BUS_Product
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint NameMaxLength = 30;
        public const uint PriceMaxValue = 100000000;

        public static Product[] ReadList(string sKeyword)
        {
            /* Return product list */
            return DAL_Product.ReadList(sKeyword);
        }

        public static void UpdateBrand(Brand brandOld, Brand brandNew)
        {
            DAL_Product.UpdateBrand(brandOld, brandNew);
        }

        public static void UpdateCategory(Category categoryOld, Category categoryNew)
        {
            DAL_Product.UpdateCategory(categoryOld, categoryNew);
        }

        public static string Add(string sID, string sName, string sCategory, string sBrand, string sPrice, ref Product product)
        {
            product.ID = sID;
            product.Name = sName;
            product.Category = sCategory;
            product.Brand = sBrand;

            /* Check for valid product ID length */
            if (product.ID.Length == 0 || product.ID.Length > IDMaxLength)
            {
                return "Product ID is invalid";
            }

            /* Check for valid product name length */
            if (product.ID.Length == 0 || product.ID.Length > NameMaxLength)
            {
                return "Product name is invalid";
            }

            /* Check for valid category */
            Category[] categoryList = BUS_Category.ReadList("");
            bool isValid = false;
            for (uint i = 0; i < categoryList.Length; i++)
            {
                if (product.Category == categoryList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Category does not exist";
            }

            /* Check for valid brand */
            Brand[] brandList = BUS_Brand.ReadList("");
            isValid = false;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (product.Brand == brandList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Brand does not exist";
            }

            /* Check for valid product price */
            if (false == uint.TryParse(sPrice, out product.Price))
            {
                return "Product price is invalid";
            }
            if (product.Price == 0 || product.Price > PriceMaxValue)
            {
                return "Product price is invalid";
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Product.Add(product);
        }

        public static string Edit(string sID, string sName, string sCategory, string sBrand, string sPrice, ref Product productOld)
        {
            Product product;
            product.ID = sID;
            product.Name = sName;
            product.Category = sCategory;
            product.Brand = sBrand;

            /* Check for valid product ID length */
            if (product.ID.Length == 0 || product.ID.Length > IDMaxLength)
            {
                return "Product ID is invalid";
            }

            /* Check for valid product name length */
            if (product.Name.Length == 0 || product.Name.Length > NameMaxLength)
            {
                return "Product name is invalid";
            }

            /* Check for valid category */
            Category[] categoryList = BUS_Category.ReadList("");
            bool isValid = false;
            for (uint i = 0; i < categoryList.Length; i++)
            {
                if (product.Category == categoryList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Category does not exist";
            }

            /* Check for valid brand */
            Brand[] brandList = BUS_Brand.ReadList("");
            isValid = false;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (product.Brand == brandList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Brand does not exist";
            }

            /* Check for valid product price */
            if (false == uint.TryParse(sPrice, out product.Price))
            {
                return "Product price is invalid";
            }
            if (product.Price == 0 || product.Price > PriceMaxValue)
            {
                return "Product price is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Product.Edit(productOld, product);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Update storage list */
                BUS_Storage.UpdateList(productOld, product);

                /* Update import list */
                BUS_Import.UpdateList(productOld, product);

                /* Update export list */
                BUS_Export.UpdateList(productOld, product);

                /* Update product old */
                productOld = product;
            }

            return sInfo;
        }

        public static string Delete(string sID, string sName, string sCategory, string sBrand, string sPrice)
        {
            Product product;
            product.ID = sID;
            product.Name = sName;
            product.Category = sCategory;
            product.Brand = sBrand;
            
            /* Check for valid product ID length */
            if (product.ID.Length == 0 || product.ID.Length > IDMaxLength)
            {
                return "Product ID is invalid";
            }

            /* Check for valid product name length */
            if (product.ID.Length == 0 || product.ID.Length > NameMaxLength)
            {
                return "Product name is invalid";
            }

            /* Check for valid category */
            Category[] categoryList = BUS_Category.ReadList("");
            bool isValid = false;
            for (uint i = 0; i<categoryList.Length; i++)
            {
                if (product.Category == categoryList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }   
            if (false == isValid)
            {
                return "Category does not exist";
            }

            /* Check for valid brand */
            Brand[] brandList = BUS_Brand.ReadList("");
            isValid = false;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (product.Brand == brandList[i].Name)
                {
                    isValid = true;
                    break;
                }
            }
            if (false == isValid)
            {
                return "Brand does not exist";
            }

            /* Check for valid product price */
            if (false == uint.TryParse(sPrice, out product.Price))
            {
                return "Product price is invalid";
            }
            if (product.Price == 0 || product.Price > PriceMaxValue)
            {
                return "Product price is invalid";
            }

            /* Check products used in import orders */
            if (string.IsNullOrEmpty(BUS_Import.ProductIsExist(product)))
            {
                return "Unable to delete products used in import orders";
            }

            /* Check products used in export orders */
            if (string.IsNullOrEmpty(BUS_Export.ProductIsExist(product)))
            {
                return "Unable to delete products used in export orders";
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Product.Delete(product);
        }

        public static Product? ReadInfo(string sProductID)
        {
            return DAL_Product.ReadInfo(sProductID);
        }
    }
}
