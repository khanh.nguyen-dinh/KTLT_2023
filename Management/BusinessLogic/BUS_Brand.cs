﻿using Management.DataAccess;
using Management.Entities;
using System.Security.Cryptography;
using System.Xml.Linq;

namespace Management.BusinessLogic
{
    public class BUS_Brand
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint NameMaxLength = 30;

        public static Brand[] ReadList(string sKeyword)
        {
            /* Return brand list */
            return DAL_Brand.ReadList(sKeyword);
        }

        public static string Add(Brand brand)
        {
            /* Check the validity of the category */
            if (brand.ID.Length > IDMaxLength)
            {
                return "Invalid brand ID";
            }
            if (brand.Name.Length > NameMaxLength)
            {
                return "Invalid brand name";
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Brand.Add(brand);
        }

        public static string Edit(string sID, string sName, ref Brand brandOld)
        {
            Brand brand;
            brand.ID = sID;
            brand.Name = sName;

            /* Check for valid Brand ID length */
            if (brand.ID.Length == 0 || brand.ID.Length > IDMaxLength)
            {
                return "Brand ID is invalid";
            }

            /* Check for valid Brand name length */
            if (brand.Name.Length == 0 || brand.Name.Length > NameMaxLength)
            {
                return "Brand name is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Brand.Edit(brandOld, brand);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Update product list */
                BUS_Product.UpdateBrand(brandOld, brand);

                /* Update brand old */
                brandOld = brand;
            }

            return sInfo;
        }

        public static string Delete(string sID, string sName)
        {
            Brand brand;
            brand.ID = sID;
            brand.Name = sName;

            /* Check for valid brand ID length */
            if (brand.ID.Length == 0 || brand.ID.Length > IDMaxLength)
            {
                return "Brand ID is invalid";
            }

            /* Check for valid brand name length */
            if (brand.ID.Length == 0 || brand.ID.Length > NameMaxLength)
            {
                return "Brand name is invalid";
            }

            /* Check all products under the brand are used in orders */
            Product[] productList = BUS_Product.ReadList("");
            for (uint i = 0; i < productList.Length; i ++)
            {
                if (brand.Name == productList[i].Brand)
                {
                    /* Check products used in import orders */
                    if (string.IsNullOrEmpty(BUS_Import.ProductIsExist(productList[i])))
                    {
                        return "Unable to delete brand has products used in import orders";
                    }

                    /* Check products used in export orders */
                    if (string.IsNullOrEmpty(BUS_Export.ProductIsExist(productList[i])))
                    {
                        return "Unable to delete brand has products used in export orders";
                    }

                    /* Check product */
                    return "Unable to delete brand has at least one product";
                }
            }

            /* Returns an empty string in case of successful addition */
            return DAL_Brand.Delete(brand);
        }

        public static Brand? ReadInfo(string brandID)
        {
            return DAL_Brand.ReadInfo(brandID);
        }
    }
}
