﻿using Management.DataAccess;
using Management.Entities;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography;

namespace Management.BusinessLogic
{
    public class BUS_Export
    {
        /* Constants */
        public const uint IDMaxLength = 30;
        public const uint UserMaxLength = 30;

        public static Export[] ReadList(string sKeyword)
        {
            /* Return export list */
            return DAL_Export.ReadList(sKeyword);
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            DAL_Export.UpdateList(productOld, productNew);
        }

        public static string ProductIsExist(Product product)
        {
            /* Read export list */
            Export[] exportList = ReadList("");

            /* Check product name in list */
            for (uint i = 0; i < exportList.Length; i++)
            {
                for (uint k = 0; k < exportList[i].Storage.Length; k++)
                {
                    if (product.Name == exportList[i].Storage[k].ProductName)
                    {
                        return string.Empty;
                    }
                }
            }

            /* product not exist */
            return "The Product is not exists";
        }

        public static string Add(string sID, string sUser, string sDate, Storage[] storageList, ref Export export)
        {
            export.ID = sID;
            export.User = sUser;
            export.Storage = storageList;

            /* Check for valid Export ID length */
            if (export.ID.Length == 0 || export.ID.Length > IDMaxLength)
            {
                return "Export ID is invalid";
            }

            /* Check for valid Export User length */
            if (export.User.Length == 0 || export.User.Length > IDMaxLength)
            {
                return "Export ID is invalid";
            }

            /* Check for valid Export Date */
            if (false == DateOnly.TryParse(sDate, out export.Date))
            {
                return "Export Date is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Export.Add(export);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Add import order to storage list */
                BUS_Storage.Export(export);
            }

            return string.Empty;
        }

        public static string Edit(string sID, string sUser, String sDate, ref Export exportOld)
        {
            Export export = new Export();
            export.ID = sID;
            export.User = sUser;

            /* Check for valid Export ID length */
            if (export.ID.Length == 0 || export.ID.Length > IDMaxLength)
            {
                return "Export ID is invalid";
            }

            /* Check for valid Export User length */
            if (export.User.Length == 0 || export.User.Length > IDMaxLength)
            {
                return "Export ID is invalid";
            }

            /* Check for valid Export Date */
            if (false == DateOnly.TryParse(sDate, out export.Date))
            {
                return "Export Date is invalid";
            }

            /* Returns an empty string in case of successful addition */
            string sInfo = DAL_Export.Edit(exportOld, export);
            if (string.IsNullOrEmpty(sInfo))
            {
                /* Update Import old */
                exportOld = export;
            }

            return sInfo;
        }

        public static string Delete(Export export)
        {
            /* Returns an empty string in case of successful addition */
            return DAL_Export.Delete(export);
        }

        public static Export? ReadInfo(string exportID)
        {
            return DAL_Export.ReadInfo(exportID);
        }
    }
}
