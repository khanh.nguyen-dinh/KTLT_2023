﻿namespace Management.Entities
{
    public struct Export
    {
        public string ID;
        public string User;
        public DateOnly Date;
        public Storage[] Storage;
    }
}
