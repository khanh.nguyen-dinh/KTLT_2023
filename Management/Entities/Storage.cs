﻿namespace Management.Entities
{
    public struct Storage
    {
        public string ProductName;
        public uint Quantity;
        public DateOnly ManufactureDate;
        public DateOnly ExpiryDate;
    }
}
