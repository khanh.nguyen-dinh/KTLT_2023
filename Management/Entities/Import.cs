﻿namespace Management.Entities
{
    public struct Import
    {
        public string ID;
        public string User;
        public DateOnly Date;
        public Storage[] Storage;
    }
}
