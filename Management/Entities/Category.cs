﻿namespace Management.Entities
{
    public struct Category
    {
        public string ID;
        public string Name;
    }
}
