﻿namespace Management.Entities
{
    public struct Product
    {
        public string ID;
        public string Name;
        public string Category;
        public string Brand;
        public uint Price;
    }
}
