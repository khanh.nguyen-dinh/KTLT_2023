﻿namespace Management.Entities
{
    public struct Brand
    {
        public string ID;
        public string Name;
    }
}
