﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Category
    {
        /* Specifies the file path for category management */
        public const string categoryFile = "~/DataBase/Category.json";

        public static Category[] ReadList(string sKeyword)
        {
            /* Count the number of categorys in the file */
            StreamReader reader = new StreamReader(categoryFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read category list from file */
            Category[] categoryList = new Category[count];
            reader = new StreamReader(categoryFile);
            for (int i = 0; i < categoryList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    categoryList[i] = JsonConvert.DeserializeObject<Category>(sData);
                }
            }
            reader.Close();

            /* Returns all categories in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return categoryList;
            }

            /* Count the number of categorys that match the keyword in the category list */
            count = 0;
            for (uint i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID.Contains(sKeyword) ||
                    categoryList[i].Name.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Category[] searchList = new Category[count];
            count = 0;
            for (uint i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID.Contains(sKeyword) ||
                    categoryList[i].Name.Contains(sKeyword))
                {
                    searchList[count++] = categoryList[i];
                }
            }

            return searchList;
        }

        public static void SaveList(Category[] categoryList)
        {
            /* Write category information to file */
            StreamWriter writer = new StreamWriter(categoryFile);
            foreach (Category category in categoryList)
            {
                string sData = JsonConvert.SerializeObject(category);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public static string Add(Category category)
        {
            /* Get category list */
            Category[] categoryList = ReadList("");

            /* The category must not yet exist */
            for (uint i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID == category.ID)
                {
                    return "The Category ID already exists";
                }
                if (categoryList[i].Name == category.Name)
                {
                    return "The Category Name already exists";
                }
            }

            /* Add new category to the top of the list */
            Category[] categoryListNew = new Category[categoryList.Length + 1];
            categoryListNew[0] = category;

            /* Add all old categorys */
            for (int i = 0; i < categoryList.Length; i++)
            {
                categoryListNew[i + 1] = categoryList[i];
            }

            /* Save new list */
            SaveList(categoryListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Edit(Category categoryOld, Category categoryNew)
        {
            /* Get category list */
            Category[] categoryList = ReadList("");

            /* The Category must exist */
            bool IsExist = false;
            for (int i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID == categoryOld.ID || categoryList[i].Name == categoryOld.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Category is not exists";
            }

            /* The new category must not yet exist */
            for (int i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID == categoryNew.ID && categoryOld.ID != categoryNew.ID)
                {
                    return "New Category ID already exists";
                }
                if (categoryList[i].Name == categoryNew.Name && categoryOld.Name != categoryNew.Name)
                {
                    return "New Category name already exists";
                }
            }

            /* Update category */
            for (int i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID == categoryOld.ID || categoryList[i].Name == categoryOld.Name)
                {
                    categoryList[i] = categoryNew;
                    break;
                }
            }

            /* Save list */
            SaveList(categoryList);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Delete(Category category)
        {
            /* Get category list */
            Category[] categoryList = ReadList("");

            /* The Category must exist */
            bool IsExist = false;
            for (int i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID == category.ID || categoryList[i].Name == category.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Category is not exists";
            }

            /* Remove category from the list */
            Category[] categoryListNew = new Category[categoryList.Length - 1];
            uint count = 0;
            for (int i = 0; i < categoryList.Length; i++)
            {
                if (categoryList[i].ID != category.ID && categoryList[i].Name != category.Name)
                {
                    categoryListNew[count] = categoryList[i];
                    count ++;
                }
            }

            /* Save new list */
            SaveList(categoryListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static Category? ReadInfo(string categoryCode)
        {
            /* Get category list */
            Category[] categoryList = ReadList("");

            /* Search by Category ID in the list */
            foreach (Category category in categoryList) 
            {
                if (categoryCode == category.ID)
                {
                    return category;
                }
            }

            /* Not found */
            return null;
        }
    }
}
