﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Brand
    {
        /* Specifies the file path for brand management */
        public const string brandFile = "~/DataBase/Brand.json";

        public static Brand[] ReadList(string sKeyword)
        {
            /* Count the number of brands in the file */
            StreamReader reader = new StreamReader(brandFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read brand list from file */
            Brand[] brandList = new Brand[count];
            reader = new StreamReader(brandFile);
            for (int i = 0; i < brandList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    brandList[i] = JsonConvert.DeserializeObject<Brand>(sData);
                }
            }
            reader.Close();

            /* Returns all brands in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return brandList;
            }

            /* Count the number of brands that match the keyword in the brand list */
            count = 0;
            for (uint i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID.Contains(sKeyword) ||
                    brandList[i].Name.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Brand[] searchList = new Brand[count];
            count = 0;
            for (uint i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID.Contains(sKeyword) ||
                    brandList[i].Name.Contains(sKeyword))
                {
                    searchList[count++] = brandList[i];
                }
            }

            return searchList;
        }

        public static void SaveList(Brand[] brandList)
        {
            /* Write brand information to file */
            StreamWriter writer = new StreamWriter(brandFile);
            foreach (Brand brand in brandList)
            {
                string sData = JsonConvert.SerializeObject(brand);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public static string Add(Brand brand)
        {
            /* Get brand list */
            Brand[] brandList = ReadList("");

            /* The brand must not yet exist */
            for (uint i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID == brand.ID)
                {
                    return "The Brand ID already exists";
                }
                if (brandList[i].Name == brand.Name)
                {
                    return "The Brand Name already exists";
                }
            }

            /* Add new brand to the top of the list */
            Brand[] brandListNew = new Brand[brandList.Length + 1];
            brandListNew[0] = brand;

            /* Add all old brands */
            for (int i = 0; i < brandList.Length; i++)
            {
                brandListNew[i + 1] = brandList[i];
            }

            /* Save new list */
            SaveList(brandListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Edit(Brand brandOld, Brand brandNew)
        {
            /* Get brand list */
            Brand[] brandList = ReadList("");

            /* The Brand must exist */
            bool IsExist = false;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID == brandOld.ID || brandList[i].Name == brandOld.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Brand is not exists";
            }

            /* The new brand must not yet exist */
            for (int i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID == brandNew.ID && brandOld.ID != brandNew.ID)
                {
                    return "New Brand ID already exists";
                }
                if (brandList[i].Name == brandNew.Name && brandOld.Name != brandNew.Name)
                {
                    return "New Brand name already exists";
                }
            }

            /* Update brand */
            for (uint i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID == brandOld.ID || brandList[i].Name == brandOld.Name)
                {
                    brandList[i] = brandNew;
                    break;
                }
            }

            /* Save list */
            SaveList(brandList);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Delete(Brand brand)
        {
            /* Get brand list */
            Brand[] brandList = ReadList("");

            /* The Brand must exist */
            bool IsExist = false;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID == brand.ID || brandList[i].Name == brand.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Brand is not exists";
            }

            /* Remove brand from the list */
            Brand[] brandListNew = new Brand[brandList.Length - 1];
            uint count = 0;
            for (int i = 0; i < brandList.Length; i++)
            {
                if (brandList[i].ID != brand.ID && brandList[i].Name != brand.Name)
                {
                    brandListNew[count] = brandList[i];
                    count ++;
                }
            }

            /* Save new list */
            SaveList(brandListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static Brand? ReadInfo(string brandID)
        {
            /* Get brand list */
            Brand[] brandList = ReadList("");

            /* Search by Brand ID in the list */
            foreach (Brand brand in brandList) 
            {
                if (brandID == brand.ID)
                {
                    return brand;
                }
            }

            /* Not found */
            return null;
        }
    }
}
