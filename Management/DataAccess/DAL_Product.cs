﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Product
    {
        /* Specifies the file path for product management */
        public const string productFile = "~/DataBase/Product.json";

        public static Product[] ReadList(string sKeyword)
        {
            /* Count the number of products in the file */
            StreamReader reader = new StreamReader(productFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read product list from file */
            Product[] productList = new Product[count];
            reader = new StreamReader(productFile);
            for (int i = 0; i < productList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    productList[i] = JsonConvert.DeserializeObject<Product>(sData);
                }
            }
            reader.Close();

            /* Returns all products in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return productList;
            }

            /* Count the number of categorys that match the keyword in the category list */
            count = 0;
            for (uint i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID.Contains(sKeyword) ||
                    productList[i].Name.Contains(sKeyword) ||
                    productList[i].Category.Contains(sKeyword) ||
                    productList[i].Brand.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Product[] searchList = new Product[count];
            count = 0;
            for (uint i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID.Contains(sKeyword) ||
                    productList[i].Name.Contains(sKeyword) ||
                    productList[i].Category.Contains(sKeyword) ||
                    productList[i].Brand.Contains(sKeyword))
                {
                    searchList[count++] = productList[i];
                }
            }

            return searchList;
        }

        public static void SaveList(Product[] productList)
        {
            /* Write product information to file */
            StreamWriter writer = new StreamWriter(productFile);
            foreach (Product product in productList)
            {
                string sData = JsonConvert.SerializeObject(product);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public static void UpdateBrand(Brand brandOld, Brand brandNew)
        {
            /* Read product list */
            Product[] productList = ReadList("");

            /* Replace old brand with new brand */
            for (uint i = 0; i < productList.Length; i ++)
            {
                if (brandOld.Name == productList[i].Brand)
                {
                    productList[i].Brand = brandNew.Name;
                }
            }

            /* Save new list to file */
            SaveList(productList);
        }

        public static void UpdateCategory(Category categoryOld, Category categoryNew)
        {
            /* Read product list */
            Product[] productList = ReadList("");

            /* Replace old category with new category */
            for (uint i = 0; i < productList.Length; i++)
            {
                if (categoryOld.Name == productList[i].Category)
                {
                    productList[i].Category = categoryNew.Name;
                }
            }

            /* Save new list to file */
            SaveList(productList);
        }

        public static string Add(Product product)
        {
            /* Get product list */
            Product[] productList = ReadList("");

            /* The product must not yet exist */
            for (int i = 0; i < productList.Length; i ++)
            {
                if (productList[i].ID == product.ID)
                {
                    return "The Product ID already exists";
                }
                if (productList[i].Name == product.Name)
                {
                    return "The Product Name already exists";
                }
            }

            /* Add new product to the top of the list */
            Product[] productListNew = new Product[productList.Length + 1];
            productListNew[0] = product;

            /* Add all old products */
            for (int i = 0; i < productList.Length; i ++)
            {
                productListNew[i + 1] = productList[i];
            }

            /* Save new list */
            SaveList(productListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Edit(Product productOld, Product productNew)
        {
            /* Get product list */
            Product[] productList = ReadList("");

            /* Old Product must exist */
            bool IsExist = false;
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID == productOld.ID || productList[i].Name == productOld.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "Product is not exists";
            }

            /* The new product must not yet exist */
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID == productNew.ID && productOld.ID != productNew.ID)
                {
                    return "New Product ID already exists";
                }
                if (productList[i].Name == productNew.Name && productOld.Name != productNew.Name)
                {
                    return "New Product Name already exists";
                }
            }

            /* Update product */
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID == productOld.ID || productList[i].Name == productOld.Name)
                {
                    productList[i] = productNew;
                    break;
                }
            }

            /* Save list */
            SaveList(productList);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Delete(Product product)
        {
            /* Get product list */
            Product[] productList = ReadList("");

            /* The Product must exist */
            bool IsExist = false;
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID == product.ID || productList[i].Name == product.Name)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Product is not exists";
            }

            /* Remove product from the list */
            Product[] productListNew = new Product[productList.Length - 1];
            uint count = 0;
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].ID != product.ID && productList[i].Name != product.Name)
                {
                    productListNew[count] = productList[i];
                    count ++;
                }
            }

            /* Save new list */
            SaveList(productListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static Product? ReadInfo(string sProductID)
        {
            /* Get product list */
            Product[] productList = ReadList("");

            /* Search by Product ID in the list */
            foreach (Product product in productList) 
            {
                if (sProductID == product.ID)
                {
                    return product;
                }
            }

            /* Not found */
            return null;
        }
    }
}
