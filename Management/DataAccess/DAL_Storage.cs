﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Storage
    {
        /* Specifies the file path for storage management */
        public const string storageFile = "~/DataBase/Storage.json";

        public static Storage[] ReadList(string sKeyword)
        {
            /* Count the number of storages in the file */
            StreamReader reader = new StreamReader(storageFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read storage list from file */
            Storage[] storageList = new Storage[count];
            reader = new StreamReader(storageFile);
            for (int i = 0; i < storageList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    storageList[i] = JsonConvert.DeserializeObject<Storage>(sData);
                }
            }
            reader.Close();

            /* Returns all storages in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return storageList;
            }

            /* Count the number of storages that match the keyword in the storage list */
            count = 0;
            for (uint i = 0; i < storageList.Length; i++)
            {
                if (storageList[i].ProductName.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Storage[] searchList = new Storage[count];
            count = 0;
            for (uint i = 0; i < storageList.Length; i++)
            {
                if (storageList[i].ProductName.Contains(sKeyword))
                {
                    searchList[count++] = storageList[i];
                }
            }

            return searchList;
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            /* Read all storage list */
            Storage[] storageList = ReadList("");

            /* Replace old products with new products */
            for (uint i = 0; i < storageList.Length; i ++)
            {
                if (productOld.Name == storageList[i].ProductName)
                {
                    storageList[i].ProductName = productNew.Name;
                }
            }

            /* Save new list to file */
            SaveList(storageList);
        }

        public static void Import(Import import)
        {
            /* Read all storage list */
            Storage[] storageList = ReadList("");

            /* Recount quantity of existing products and count number of new products */
            bool isExist = false;
            uint count = 0;
            for (uint i = 0; i < import.Storage.Length; i ++)
            {
                for (uint k = 0; k < storageList.Length; k ++)
                {
                    /* Check product is exist */
                    if (import.Storage[i].ProductName == storageList[k].ProductName &&
                        import.Storage[i].ManufactureDate == storageList[k].ManufactureDate &&
                        import.Storage[i].ExpiryDate == storageList[k].ExpiryDate)
                    {
                        /* Recount quantity of existing products */
                        storageList[k].Quantity += import.Storage[i].Quantity;

                        /* Confirm this product is already exist */
                        isExist = true;
                        break;
                    }
                }

                /* Count number of new products */
                if (false == isExist)
                {
                    count++;
                }

                /* Clean the flag for the next check */
                isExist = false;
            } 
            /* Has new products */
            if (0 < count)
            {
                /* Add new products to the top of the list */
                Storage[] storageListNew = new Storage[storageList.Length + count];
                for (uint i = 0; i < count; i ++)
                {
                    for (uint k = 0; k < storageList.Length; k++)
                    {
                        /* Check product is exist */
                        if (import.Storage[i].ProductName == storageList[k].ProductName &&
                            import.Storage[i].ManufactureDate == storageList[k].ManufactureDate &&
                            import.Storage[i].ExpiryDate == storageList[k].ExpiryDate)
                        {
                            /* Confirm this product is already exist */
                            isExist = true;
                            break;
                        }
                    }

                    /* Add new products to new list */
                    if (false == isExist)
                    {
                        storageListNew[i] = import.Storage[i];
                    }

                    /* Clean the flag for the next check */
                    isExist = false;
                }

                /* add old products to new list */
                for (uint i = 0; i < storageList.Length; i++)
                {
                    storageListNew[i + count] = storageList[i];
                }

                /* Save new list */
                storageList = storageListNew;
            }

            /* Save new list to file */
            SaveList(storageList);
        }

        public static void Export(Export export)
        {
            /* Read all storage list */
            Storage[] storageList = ReadList("");

            /* Recount quantity of existing products */
            for (uint i = 0; i < export.Storage.Length; i++)
            {
                for (uint k = 0; k < storageList.Length; k++)
                {
                    /* Check product is exist */
                    if (export.Storage[i].ProductName == storageList[k].ProductName &&
                        export.Storage[i].ManufactureDate == storageList[k].ManufactureDate &&
                        export.Storage[i].ExpiryDate == storageList[k].ExpiryDate)
                    {
                        /* Recount quantity of existing products */
                        storageList[k].Quantity -= export.Storage[i].Quantity;
                        break;
                    }
                }
            }

            /* Count the number of products in stock */
            uint count = 0;
            for (uint i = 0; i < storageList.Length; i ++)
            {
                if (storageList[i].Quantity != 0)
                {
                    count ++;
                }
            }

            /* Remove out of stock products */
            if (count > 0)
            {
                Storage[] storageListNew = new Storage[count];
                count = 0;
                for (uint i = 0; i < storageList.Length; i++)
                {
                    if (storageList[i].Quantity != 0)
                    {
                        storageListNew[count] = storageList[i];
                        count++;
                    }
                }

                /* Save new list */
                storageList = storageListNew;
            }

            /* Save new list to file */
            SaveList(storageList);
        }

        public static void SaveList(Storage[] storageList)
        {
            /* Write storage information to file */
            StreamWriter writer = new StreamWriter(storageFile);
            foreach (Storage storage in storageList)
            {
                string sData = JsonConvert.SerializeObject(storage);
                writer.WriteLine(sData);
            }
            writer.Close();
        }
    }
}
