﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Export
    {
        /* Specifies the file path for export management */
        public const string exportFile = "~/DataBase/Export.json";

        public static Export[] ReadList(string sKeyword)
        {
            /* Count the number of exports in the file */
            StreamReader reader = new StreamReader(exportFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read export list from file */
            Export[] exportList = new Export[count];
            reader = new StreamReader(exportFile);
            for (int i = 0; i < exportList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    exportList[i] = JsonConvert.DeserializeObject<Export>(sData);
                }
            }
            reader.Close();

            /* Returns all exports in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return exportList;
            }

            /* Count the number of exports that match the keyword in the export list */
            count = 0;
            for (uint i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID.Contains(sKeyword) ||
                    exportList[i].User.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Export[] searchList = new Export[count];
            count = 0;
            for (uint i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID.Contains(sKeyword) ||
                    exportList[i].User.Contains(sKeyword))
                {
                    searchList[count++] = exportList[i];
                }
            }

            return searchList;
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            /* Read all storage list */
            Export[] exportList = ReadList("");

            /* Replace old products with new products */
            for (uint i = 0; i < exportList.Length; i++)
            {
                for (uint k = 0; k < exportList[i].Storage.Length; k++)
                {
                    if (productOld.Name == exportList[i].Storage[k].ProductName)
                    {
                        exportList[i].Storage[k].ProductName = productNew.Name;
                    }
                }
            }

            /* Save new list to file */
            SaveList(exportList);
        }

        public static void SaveList(Export[] exportList)
        {
            /* Write export information to file */
            StreamWriter writer = new StreamWriter(exportFile);
            foreach (Export export in exportList)
            {
                string sData = JsonConvert.SerializeObject(export);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public static string Add(Export export)
        {
            /* Get export list */
            Export[] exportList = ReadList("");

            /* The export must not yet exist */
            for (uint i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID == export.ID)
                {
                    return "The Export ID already exists";
                }
            }

            /* Add new export to the top of the list */
            Export[] exportListNew = new Export[exportList.Length + 1];
            exportListNew[0] = export;

            /* Add all old exports */
            for (int i = 0; i < exportList.Length; i++)
            {
                exportListNew[i + 1] = exportList[i];
            }

            /* Save new list */
            SaveList(exportListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Edit(Export exportOld, Export exportNew)
        {
            /* Get export list */
            Export[] exportList = ReadList("");

            /* The Export must exist */
            bool IsExist = false;
            for (int i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID == exportOld.ID)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Export is not exists";
            }

            /* The new export must not yet exist */
            for (int i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID == exportNew.ID && exportOld.ID != exportNew.ID)
                {
                    return "The Export ID already exists";
                }
            }

            /* Update export */
            for (int i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID == exportOld.ID || exportList[i].User == exportOld.User)
                {
                    exportList[i].ID = exportNew.ID;
                    exportList[i].User = exportNew.User;
                    exportList[i].Date = exportNew.Date;
                    break;
                }
            }

            /* Save list */
            SaveList(exportList);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Delete(Export export)
        {
            /* Get export list */
            Export[] exportList = ReadList("");

            /* The Export must exist */
            bool IsExist = false;
            for (int i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID == export.ID || exportList[i].User == export.User)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Export is not exists";
            }

            /* Remove export from the list */
            Export[] exportListNew = new Export[exportList.Length - 1];
            uint count = 0;
            for (int i = 0; i < exportList.Length; i++)
            {
                if (exportList[i].ID != export.ID && exportList[i].User != export.User)
                {
                    exportListNew[count] = exportList[i];
                    count ++;
                }
            }

            /* Save new list */
            SaveList(exportListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static Export? ReadInfo(string exportCode)
        {
            /* Get export list */
            Export[] exportList = ReadList("");

            /* Search by Export ID in the list */
            foreach (Export export in exportList) 
            {
                if (exportCode == export.ID)
                {
                    return export;
                }
            }

            /* Not found */
            return null;
        }
    }
}
