﻿using Management.Entities;
using Newtonsoft.Json;

namespace Management.DataAccess
{
    public class DAL_Import
    {
        /* Specifies the file path for import management */
        public const string importFile = "~/DataBase/Import.json";

        public static Import[] ReadList(string sKeyword)
        {
            /* Count the number of imports in the file */
            StreamReader reader = new StreamReader(importFile);
            uint count = 0;
            while (null != reader.ReadLine())
            {
                count++;
            }
            reader.Close();

            /* Read import list from file */
            Import[] importList = new Import[count];
            reader = new StreamReader(importFile);
            for (uint i = 0; i < importList.Length; i++)
            {
                string? sData = reader.ReadLine();
                if (null != sData)
                {
                    importList[i] = JsonConvert.DeserializeObject<Import>(sData);
                }
            }
            reader.Close();

            /* Returns all imports in the list if the keyword is not present */
            if (string.IsNullOrEmpty(sKeyword))
            {
                return importList;
            }

            /* Count the number of imports that match the keyword in the import list */
            count = 0;
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID.Contains(sKeyword) ||
                    importList[i].User.Contains(sKeyword))
                {
                    count++;
                }
            }

            /* Get search list */
            Import[] searchList = new Import[count];
            count = 0;
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID.Contains(sKeyword) ||
                    importList[i].User.Contains(sKeyword))
                {
                    searchList[count++] = importList[i];
                }
            }

            return searchList;
        }

        public static void UpdateList(Product productOld, Product productNew)
        {
            /* Read all storage list */
            Import[] importList = ReadList("");

            /* Replace old products with new products */
            for (uint i = 0; i < importList.Length; i++)
            {
                for (uint k = 0; k < importList[i].Storage.Length; k ++)
                {
                    if (productOld.Name == importList[i].Storage[k].ProductName)
                    {
                        importList[i].Storage[k].ProductName = productNew.Name;
                    }
                }
            }

            /* Save new list to file */
            SaveList(importList);
        }

        public static void SaveList(Import[] importList)
        {
            /* Write import information to file */
            StreamWriter writer = new StreamWriter(importFile);
            foreach (Import import in importList)
            {
                string sData = JsonConvert.SerializeObject(import);
                writer.WriteLine(sData);
            }
            writer.Close();
        }

        public static string Add(Import import)
        {
            /* Get import list */
            Import[] importList = ReadList("");

            /* The import must not yet exist */
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID == import.ID)
                {
                    return "The Import ID already exists";
                }
            }

            /* Add new import to the top of the list */
            Import[] importListNew = new Import[importList.Length + 1];
            importListNew[0] = import;

            /* Add all old imports */
            for (uint i = 0; i < importList.Length; i++)
            {
                importListNew[i + 1] = importList[i];
            }

            /* Save new list */
            SaveList(importListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Edit(Import importOld, Import importNew)
        {
            /* Get import list */
            Import[] importList = ReadList("");

            /* The Import must exist */
            bool IsExist = false;
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID == importOld.ID)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Import is not exists";
            }

            /* The new import must not yet exist */
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID == importNew.ID && importOld.ID != importNew.ID)
                {
                    return "The Import ID already exists";
                }
            }

            /* Update import */
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID == importOld.ID || importList[i].User == importOld.User)
                {
                    importList[i].ID = importNew.ID;
                    importList[i].User = importNew.User;
                    importList[i].Date = importNew.Date;
                    break;
                }
            }

            /* Save list */
            SaveList(importList);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static string Delete(Import import)
        {
            /* Get import list */
            Import[] importList = ReadList("");

            /* The Import must exist */
            bool IsExist = false;
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID == import.ID || importList[i].User == import.User)
                {
                    IsExist = true;
                    break;
                }
            }
            if (false == IsExist)
            {
                return "The Import is not exists";
            }

            /* Remove import from the list */
            Import[] importListNew = new Import[importList.Length - 1];
            uint count = 0;
            for (uint i = 0; i < importList.Length; i++)
            {
                if (importList[i].ID != import.ID && importList[i].User != import.User)
                {
                    importListNew[count] = importList[i];
                    count ++;
                }
            }

            /* Save new list */
            SaveList(importListNew);

            /* Returns an empty string in case the operation is successful */
            return string.Empty;
        }

        public static Import? ReadInfo(string importID)
        {
            /* Get import list */
            Import[] importList = ReadList("");

            /* Search by Import ID in the list */
            foreach (Import import in importList) 
            {
                if (importID == import.ID)
                {
                    return import;
                }
            }

            /* Not found */
            return null;
        }
    }
}
